
1.
mkdir -p hello/{five/six/seven,one/two/three/four} && cd hello/five/six
touch c.txt && cd seven && touch error.log
pushd /Users/vaibhavmehta/hello/one && touch a.txt b.txt && cd two
touch d.txt && cd three && touch e.txt && cd four && touch access.log

2.
find . -name "*.log" -type f -delete

3.
pushd Users/vaibhavmehta/hello/one
nano a.txt
Unix is a family of multitasking, multiuser computer operating systems that derive from the original AT&T Unix, development starting in the 1970s at the Bell Labs research center by Ken Thompson, Dennis Ritchie, and others. 

4.
cd .. && rm -r five
